FROM php:7.2-apache

RUN apt update

COPY --from=composer:1.10.20 /usr/bin/composer /usr/local/bin/composer

RUN apt-get install zip unzip

WORKDIR /app

COPY . .

RUN composer update

RUN chmod 777 storage/ && ln -s /app/public/ /var/www/html/lumen-test && chmod 777 /var/www/html/lumen-test && a2enmod rewrite && service apache2 restart && apt-get remove zip unzip -y && apt clean

EXPOSE 80

# docker build --rm -t name:tag .

# docker run -it -d -p 3000:80 --name lumen_test -v /home/vipin/projects/lumen/lumen_api:/app  -v lumen_vendor:/app/vendor lumen_test:test
