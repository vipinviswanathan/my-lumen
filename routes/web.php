<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
// $router->get('profile', [
//     'as' => 'profile', 'uses' => 'Controller@showProfile'
// ]);
// $router->post('/', function () {
//     return ['version' => '5.3'];
// });

$router->group(['prefix' => '/'], function () use ($router) {
    $router->post('hereare', function () {
        return "are you there";
    });
});
// $router->get('keys', function () use ($router) {
//     return $router->app->version();
// });
// $router->post('./hereare', function () {
//     return "hjere";
// });
